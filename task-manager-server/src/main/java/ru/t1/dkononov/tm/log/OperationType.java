package ru.t1.dkononov.tm.log;

public enum OperationType {

    INSERT,
    DELETE,
    UPDATE;

}

