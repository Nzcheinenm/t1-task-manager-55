package ru.t1.dkononov.tm.component;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.dkononov.tm.api.endpoint.*;
import ru.t1.dkononov.tm.api.repository.ICommandRepository;
import ru.t1.dkononov.tm.api.services.*;
import ru.t1.dkononov.tm.command.AbstractCommand;
import ru.t1.dkononov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.dkononov.tm.exception.system.CommandNotSupportedException;
import ru.t1.dkononov.tm.util.SystemUtil;
import ru.t1.dkononov.tm.util.TerminalUtil;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.Map;


@Component
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMAND = "ru.t1.dkononov.tm.command";

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    @Autowired
    private AbstractCommand[] abstractCommands;

    @NotNull
    @Autowired
    private ICommandRepository commandRepository;

    @Getter
    @NotNull
    @Autowired
    private ICommandService commandService;

    @Getter
    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @Getter
    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Getter
    @NotNull
    @Autowired
    private ISystemEndpoint systemEndpointClient;

    @Getter
    @NotNull
    @Autowired
    private IDomainEndpoint domainEndpointClient;

    @Getter
    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpointClient;

    @Getter
    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpointClient;

    @Getter
    @NotNull
    @Autowired
    private IUserEndpoint userEndpointClient;

    @Getter
    @NotNull
    @Autowired
    private IAuthEndpoint authEndpoint;

    @Getter
    @NotNull
    @Autowired
    private ITokenService tokenService;

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);


    public void run(@NotNull final String[] args) {
        if (processArgument(args)) System.exit(0);
        init();
        initCommands(abstractCommands);
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.inLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    private void initCommands(AbstractCommand[] abstractCommands) {
        for (@Nullable final AbstractCommand command : abstractCommands) {
            if (command == null) return;
            commandService.add(command);
        }
    }

    private void init() {
        try {
            prepareStart();
        } catch (final Exception e) {
            loggerService.error(e);
            System.err.println("[INIT FAIL]");
        }
    }

    private void prepareStart() throws Exception {
        initPID();
        initLogger();
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        fileScanner.init();
    }

    private void prepareShutdown() {
        fileScanner.stop();
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
    }

    private void processArgument(@Nullable final String argument)
            throws Exception {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    private boolean processArgument(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String argument = args[0];
        try {
            processArgument(argument);
            return true;
        } catch (@NotNull final Exception e) {
            loggerService.error(e);
            return false;
        }
    }

    private void initPID() throws IOException {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK-MANAGER **");
    }

    public void processCommand(@Nullable final String command, final boolean checkRoles)
            throws Exception {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    void processCommand(@Nullable final String command)
            throws Exception {
        processCommand(command, true);
    }


    private void registry(@NotNull final AbstractCommand command) {
//        command.setServiceLocator(this);
        commandService.add(command);
    }

}
