package ru.t1.dkononov.tm.command.data;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.dkononov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.dkononov.tm.command.AbstractCommand;

@Component
public abstract class AbstractDataCommand extends AbstractCommand {

    @Getter
    @Setter
    @NotNull
    @Autowired
    protected IDomainEndpoint domainEndpoint;

}
