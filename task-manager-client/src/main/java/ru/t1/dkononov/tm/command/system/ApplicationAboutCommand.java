package ru.t1.dkononov.tm.command.system;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.dkononov.tm.api.services.IPropertyService;
import ru.t1.dkononov.tm.dto.request.ApplicationAboutRequest;
import ru.t1.dkononov.tm.dto.response.ApplicationAboutResponse;

@Component
public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @Getter
    @NotNull
    public final String DESCRIPTION = "Информация о разработчике";

    @Getter
    @NotNull
    public final String NAME = "about";

    @Getter
    @NotNull
    public final String ARGUMENT = "-a";


    @Override
    public void execute() throws Exception {
        @NotNull final IPropertyService service = propertyService;

        System.out.println("[GIT]");
        System.out.println("COMMIT ID: " + service.getGitCommitId());
        System.out.println("BRANCH: " + service.getGitBranch());
        System.out.println("MESSAGE: " + service.getGitCommitMessage());
        System.out.println("TIME: " + service.getGitCommitTime());
        System.out.println("COMMITER: " + service.getGitCommiterName());
        System.out.println("E-MAIL: " + service.getGitCommiterEmail());
        System.out.println();

        @NotNull final ApplicationAboutRequest request = new ApplicationAboutRequest();
        @NotNull final ApplicationAboutResponse serverAboutResponse = systemEndpoint.getAbout(request);

        System.out.println("[ABOUT]");
        System.out.println(serverAboutResponse.getEmail());
        System.out.println(serverAboutResponse.getName());
    }

}
