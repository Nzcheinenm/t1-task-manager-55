package ru.t1.dkononov.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.dkononov.tm.component.Bootstrap;
import ru.t1.dkononov.tm.configuration.ClientConfiguration;

public final class Application {

    public static void main(@NotNull final String[] args) {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(ClientConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.run(args);
    }

}

